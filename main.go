package main

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net/http"
	"os/exec"
	"strings"
	"time"

	"golang.org/x/net/html"
)

func findDIVWithClass(n *html.Node, class string) *html.Node {
	if n.Type == html.ElementNode && n.Data == "div" {
		for _, a := range n.Attr {
			if a.Key == "class" && strings.Contains(a.Val, class) {
				return n
			}
		}
	}
	for c := n.FirstChild; c != nil; c = c.NextSibling {
		if result := findDIVWithClass(c, class); result != nil {
			return result
		}
	}
	return nil
}

func fetchURL(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	doc, err := html.Parse(bytes.NewReader(body))
	if err != nil {
		return "", err
	}

	div := findDIVWithClass(doc, "Page_Body")
	if div == nil {
		return "", fmt.Errorf("div with class 'page_body' not found")
	}

	var buf bytes.Buffer
	err = html.Render(&buf, div)
	if err != nil {
		return "", err
	}

	return buf.String(), nil
}

func sendNotification(message string) {
	soundFilePath := "/usr/share/sounds/freedesktop/stereo/suspend-error.oga"
	cmdSound := exec.Command("paplay", soundFilePath)
	err := cmdSound.Run()
	if err != nil {
		log.Printf("Error playing sound: %v", err)
	}

	cmd := exec.Command("notify-send", "--urgency=critical", "--icon=dialog-information", message, message)
	err = cmd.Run()
	if err != nil {
		log.Printf("Error sending notification: %v", err)
	}
}

func main() {
	url := "https://www.damascusuniversity.edu.sy/ite/index.php?func=2&set=14&College=&Category=0&lang=1&CStadyYear=&department_id=-1&StadyYear=-1&Year=2024&Season=2"
	var lastContent string

	for {
		content, err := fetchURL(url)

		if err != nil {
			log.Printf("Error fetching URL: %v\n", err)
			continue
		}

		if lastContent != "" && content != lastContent {
			sendNotification("New alamat were published.")
		} else {
			log.Println("Nothing new")
		}
		lastContent = content

		time.Sleep(15 * time.Second)
	}
}
