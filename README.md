# Go FITE Damascus University Marks

This program will notify you when new marks are published on
[https://www.damascusuniversity.edu.sy/ite/](https://www.damascusuniversity.edu.sy/ite/).

## How to use

You need [go](https://go.dev/) to be installed, and `paplay` & `notify-send`
commands to be available on your machine

```sh
go install gitlab.com/sudoCss/gofitedumarks@latest
./path/to/your/go/bin/gofitedumarks
```

OR

```sh
git clone https://gitlab.com/sudoCss/gofitedumarks.git
cd gofitedumarks/
go run .
```

and keep your terminal open and the program running..
